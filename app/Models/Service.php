<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public $incrementing = false;

    protected $fillable = [
        'id',
        'name',
    ];

    public function requests()
    {
        return $this->hasMany('App\Models\Request');
    }
}
