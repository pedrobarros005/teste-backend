<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Consumer extends Model
{
    public $incrementing = false;

    protected $fillable = [
        'id'
    ];

    public function requests()
    {
        return $this->hasMany('App\Models\Request');
    }
}
