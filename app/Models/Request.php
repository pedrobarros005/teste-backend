<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    public $incrementing = false;

    protected $fillable = [
        'time_r',
        'time_p',
        'time_k',
        'service_id',
        'consumer_id'
    ];


    public function consumers()
    {
        return $this->belongsTo('App\Models\Consumer');
    }

    public function services()
    {
        return $this->belongsTo('App\Models\Service');
    }
}
