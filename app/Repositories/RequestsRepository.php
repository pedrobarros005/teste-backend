<?php

namespace App\Repositories;

use App\Models\Request;


class RequestsRepository
{
    public function requestPerConsumer()
    {
        return Request::groupBy('consumer_id')
            ->selectRaw('consumer_id, count(*) as consumer_qtd')
            ->get('consumer_id');
    }

    public function requestPerService()
    {
        return Request::groupBy('service_id')
            ->selectRaw('service_id, count(*) as service_qtd')
            ->get('service_id');
    }

    public function avgTimes()
    {
        return Request::groupBy('service_id')
            ->selectRaw('service_id, AVG(time_r) as avg_r, AVG(time_p) as avg_p, AVG(time_k) as avg_k')
            ->get('service_id');
    }
}
