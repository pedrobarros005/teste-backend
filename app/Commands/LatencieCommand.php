<?php

namespace App\Commands;

use LaravelZero\Framework\Commands\Command;
use App\Repositories\RequestsRepository;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;

class LatencieCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'latencie {name=latencie}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(RequestsRepository $request)
    {
        $totalTime = $request->avgTimes()->toArray();

        $path = storage_path();

        $writer = WriterEntityFactory::createCSVWriter();

        $writer->openToFile($path . '/metricsLatencie.csv');

        $headers = WriterEntityFactory::createRowFromArray([
            'Serviço',
            'Requests',
            'Proxy',
            'Kong',
        ]);
        $writer->addRow($headers);


        foreach ($totalTime as $totalTimes) {
            $row = WriterEntityFactory::createRowFromArray([
                $totalTimes['service_id'],
                $totalTimes['avg_r'],
                $totalTimes['avg_p'],
                $totalTimes['avg_k'],
            ]);

            $writer->addRow($row);
        }
    }
}
