<?php

namespace App\Commands;

use LaravelZero\Framework\Commands\Command;
use App\Repositories\RequestsRepository;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;

class RequestServiceCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'request_service {name=request_service}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(RequestsRepository $request)
    {
        $services = $request->requestPerService()->toArray();

        $path = storage_path();

        $writer = WriterEntityFactory::createCSVWriter();

        $writer->openToFile($path . '/metricsService.csv');

        $headers = WriterEntityFactory::createRowFromArray([
            'Serviço',
            'Requisições',
        ]);

        $writer->addRow($headers);

        foreach ($services as $service) {
            $row = WriterEntityFactory::createRowFromArray([
                $service['service_id'],
                $service['service_qtd'],
            ]);

            $writer->addRow($row);
        }
    }
}
