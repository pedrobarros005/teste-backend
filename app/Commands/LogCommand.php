<?php

namespace App\Commands;

use App\Models\Consumer;
use App\Models\Request;
use App\Models\Service;
use Illuminate\Support\Facades\Storage;
use LaravelZero\Framework\Commands\Command;

class LogCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'log {name=log}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Lê o arquivo Log.txt e armazena informações filtradas no db';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $reader = Storage::get('/storage/logs.txt');

       $this->insert($reader);

    }

    public function insert($reader)
    {
        $lines = explode("\n",$reader);


        foreach ($lines as $line) {

            $json = json_decode($line, true);

            $consumer = Consumer::firstOrCreate(
                [
                    'id' =>  $json['authenticated_entity']['consumer_id']['uuid'],
                ],
                [
                    'id' =>  $json['authenticated_entity']['consumer_id']['uuid'],
                ]
            );

            $service = Service::firstOrCreate(
                [
                    'id' => $json['service']['id'],
                ],
                [
                    'id' => $json['service']['id'],
                    'name' => (string) $json['service']['name'],
                ]
            );

            Request::firstOrCreate(
                [
                    'time_r' => (int) $json['latencies']['request'],
                    'time_p' => (int) $json['latencies']['proxy'],
                    'time_k' => (int) $json['latencies']['kong'],
                    'service_id' => $service->id,
                    'consumer_id' => $consumer->id,
                ],
                [
                    'time_r' => (int) $json['latencies']['request'],
                    'time_p' => (int) $json['latencies']['proxy'],
                    'time_k' => (int) $json['latencies']['kong'],
                    'service_id' => $service->id,
                    'consumer_id' => $consumer->id,
                ]

            );
        }
    }
}
