<?php

namespace App\Commands;

use LaravelZero\Framework\Commands\Command;
use App\Repositories\RequestsRepository;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;

class RequestConsumerCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'request_consumer {name=request_consumer}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(RequestsRepository $request)
    {
        $consumers = $request->requestPerConsumer()->toArray();

        $path = storage_path();

        $writer = WriterEntityFactory::createCSVWriter();

        $writer->openToFile($path . '/metricsConsumer.csv');

        $headers = WriterEntityFactory::createRowFromArray([
            'Consumidor',
            'Requisições',
        ]);

        $writer->addRow($headers);

        foreach ($consumers as $consumer) {
            $row = WriterEntityFactory::createRowFromArray([
                $consumer['consumer_id'],
                $consumer['service_qtd'],

            ]);

            $writer->addRow($row);
        }
    }
}
