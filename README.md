# Teste Back-end Pedro de Barros.
Para instalar a aplicação, basta clonar o repositório na sua máquina, executando o seguinte comando no seu terminal:

    git clone git@bitbucket.org:pedrobarros005/teste-backend.git

Logo, use o seguinte comando:

    composer install

Para instalar as dependências da aplicação.

# Objetivo

Criar uma aplicação que processe o arquivo log.txt disponibilizado na pasta "storage" do projeto, e salvar as seguintes informações no banco de dados:

 - Requisições por consumidor;
 - Requisições por serviço;
 - Tempo médio request, proxy e kong por serviço;

## Processando o arquivo log.txt

Para processar o arquivo log.txt, use o seguinte comando no seu terminal:

    php teste-backend log

Ele fará com que as informações disponibilizadas dentro do log.txt sejam salvas no banco de dados assim podendo ser acessadas de maneira coerente.

## Gerando relatório de Requisições por consumidor

Para gerar o relatório com as informações de requisições por consumidor, use o seguinte comando: 

    php teste-backend request_consumer

Esse comando fará com que o número de requisições por consumidor sejam salvas dentro do arquivo metricsConsumer.csv


## Gerando relatório de Requisições por serviço

Para gerar o relatório com as informações de requisições por serviço, use o seguinte comando:

    php teste-backend request_service

Esse comando fará com que o número de requisições relacionadas aos serviços em questão, sejam salvas dentro do arquivo metricsService.csv

## Gerando relatório da média de request, proxy, kong por serviço

Para gerar esse relatório com as informações de requisições por serviço, use o seguinte comando:

    php teste-backend latencie

Esse comando fará com que as médias de request, proxy e kong de cada serviço, sejam salvas no arquivo metricsLatencie.csv




# Contato
e-mail: pedro.barros@melhorenvio.com